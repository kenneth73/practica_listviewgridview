package com.example.practica_listviewgridview.activities;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.practica_listviewgridview.R;
import com.example.practica_listviewgridview.adapters.LanguageAdapter;
import com.example.practica_listviewgridview.models.ProgrammingLanguage;

import java.util.ArrayList;
/*Click prolongado para eliminar
* Click simple para editar
*/
public class MainActivity extends AppCompatActivity {
    //* D A T A*//
    public static ArrayList<ProgrammingLanguage> data;
    //* M E N U *//
    private MenuItem btnGrid;
    private MenuItem btnList;
    private MenuItem btnAdd;
    //* V I E W S*//
    private ListView listView;
    private GridView gridView;
    //static para poder acceder desde otras clases
    public static LanguageAdapter myAdapter;

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(data.get(info.position).getName());
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    // Override MENU menu de arriba //
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.layout_menu, menu);
        btnGrid = menu.findItem(R.id.btn_grid);
        btnList = menu.findItem(R.id.btn_list);
        btnAdd = menu.findItem(R.id.btn_add);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.btn_add:
                Intent intentCreateUpdate = new Intent(MainActivity.this, CreateUpdateActivity.class);
                startActivity(intentCreateUpdate);
                return true;

            case R.id.btn_list:
                listView.setVisibility(View.VISIBLE);
                gridView.setVisibility(View.GONE);
                btnList.setVisible(false);
                btnGrid.setVisible(true);
                return true;

            case R.id.btn_grid:
                listView.setVisibility(View.GONE);
                gridView.setVisibility(View.VISIBLE);
                btnList.setVisible(true);
                btnGrid.setVisible(false);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();

        switch (item.getItemId()) {
            case R.id.delete_item:
                data.remove(info.position);
                myAdapter.notifyDataSetChanged();
                return true;
            case R.id.update_item:
                Intent intentCreateUpdate = new Intent(MainActivity.this, CreateUpdateActivity.class);
                intentCreateUpdate.putExtra("position", info.position);
                startActivity(intentCreateUpdate);

                return true;

        }

        return super.onContextItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);
        listView.setVisibility(View.INVISIBLE);
        gridView = findViewById(R.id.gridView);

        data = new ArrayList<>();
        data.add(new ProgrammingLanguage(R.mipmap.ic_launcher_backend, "Java", "Tipado"));
        data.add(new ProgrammingLanguage(R.mipmap.ic_launcher_backend, "C++", "Tipado"));
        data.add(new ProgrammingLanguage(R.mipmap.ic_launcher_frontend, "C", "Tipado"));
        data.add(new ProgrammingLanguage(R.mipmap.ic_launcher_frontend, "Bash", "Tipado"));
        data.add(new ProgrammingLanguage(R.mipmap.ic_launcher_frontend, "Kotlin", "No tipado"));
        data.add(new ProgrammingLanguage(R.mipmap.ic_launcher_frontend, "Rust", "No tipado"));
        data.add(new ProgrammingLanguage(R.mipmap.ic_launcher_frontend, "Cobol", "Tipado"));
        data.add(new ProgrammingLanguage(R.mipmap.ic_launcher_frontend, "Python", "No tipado"));
        data.add(new ProgrammingLanguage(R.mipmap.ic_launcher_frontend, "Cobol", "No tipado"));
        data.add(new ProgrammingLanguage(R.mipmap.ic_launcher_frontend, "Ruby", "Tipado"));

        myAdapter = new LanguageAdapter(this, R.layout.grid_item, data);

        listView.setAdapter(myAdapter);
        gridView.setAdapter(myAdapter);

        registerForContextMenu(listView);
        registerForContextMenu(gridView);

        //* L I S E N E R S  O L D *//
        /*gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentCreateUpdate = new Intent(MainActivity.this, CreateUpdateActivity.class);
                intentCreateUpdate.putExtra("position", position);
                startActivity(intentCreateUpdate);
            }
        });

        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                String text = "Item "+ data.get(position).getName() + " deleted.";
                data.remove(position);
                Toast.makeText(MainActivity.this, text, Toast.LENGTH_SHORT).show();
                myAdapter.notifyDataSetChanged();
                return true;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intentCreateUpdate = new Intent(MainActivity.this, CreateUpdateActivity.class);
                intentCreateUpdate.putExtra("position", position);
                startActivity(intentCreateUpdate);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                String text = "Language "+ data.get(position).getName() + " deleted";
                data.remove(position);
                Toast.makeText(MainActivity.this, text, Toast.LENGTH_SHORT).show();
                myAdapter.notifyDataSetChanged();
                return true;
            }
        });*/
    }
}