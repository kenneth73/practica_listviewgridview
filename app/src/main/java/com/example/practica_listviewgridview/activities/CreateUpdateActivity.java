package com.example.practica_listviewgridview.activities;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.practica_listviewgridview.R;
import com.example.practica_listviewgridview.models.ProgrammingLanguage;

public class CreateUpdateActivity extends AppCompatActivity {

    EditText editTextName;
    Spinner spinnerImg;
    Spinner spinnerType;
    Button btnSave;
    Button btnCancel;
    String spinnerTypeValue;
    int spinnerImgValue;
    int position; //position in array item selected for updated
    boolean isCreated = false;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.layout_menu, menu);
        MenuItem btnGrid = menu.findItem(R.id.btn_grid);
        MenuItem btnList = menu.findItem(R.id.btn_list);
        MenuItem btnAdd = menu.findItem(R.id.btn_add);
        btnList.setVisible(false);
        btnGrid.setVisible(false);
        btnAdd.setVisible(false);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_update_activity);

        editTextName = findViewById(R.id.edit_text_name);
        spinnerImg = findViewById(R.id.spinner_img);
        spinnerType = findViewById(R.id.spinner_type);
        btnSave = findViewById(R.id.btn_save);
        btnCancel = findViewById(R.id.btn_cancel);

        //* S P I N N E R S*//
        ArrayAdapter<CharSequence> typeAdapterSpin = ArrayAdapter.createFromResource(this,R.array.type, android.R.layout.simple_spinner_item);
        spinnerType.setAdapter(typeAdapterSpin);
        ArrayAdapter<CharSequence> imgAdapterSpin = ArrayAdapter.createFromResource(this,R.array.img, android.R.layout.simple_spinner_item);
        spinnerImg.setAdapter(imgAdapterSpin);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            btnSave.setText(R.string.btn_update);
            isCreated = false;
            position = bundle.getInt("position");

            // set edit text
            ProgrammingLanguage element = MainActivity.data.get(position);
            editTextName.setText(element.getName());
            // set spinner type
            if (element.getType().equalsIgnoreCase("Typing")) {
                spinnerType.setSelection(0);
            } else {
                spinnerType.setSelection(1);
            }
            // set spinner img
            int currentPhoto = element.getIdImage();
            int imgBackend = R.mipmap.ic_launcher_backend;
            int imgFrontend = R.mipmap.ic_launcher_frontend;
            if (currentPhoto == imgBackend) spinnerImg.setSelection(0);
            if (currentPhoto == imgFrontend) spinnerImg.setSelection(1);

        } else {
            btnSave.setText(R.string.btn_create);
            isCreated = true;
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextName.getText().toString();
                String type = spinnerType.getSelectedItem().toString();
                int img = 0;
                if (!name.equals("")){

                    switch (spinnerImgValue){
                        case 0: //backend
                            img = R.mipmap.ic_launcher_backend;
                            break;
                        case 1: //frontend
                            img = R.mipmap.ic_launcher_frontend;
                            break;
                    }
                } else {
                    Toast.makeText(CreateUpdateActivity.this, "Name is required", Toast.LENGTH_SHORT).show();
                }


                if (isCreated) {
                    MainActivity.data.add(new ProgrammingLanguage(img, name, type));
                } else {
                    MainActivity.data.get(position).setIdImage(img);
                    MainActivity.data.get(position).setName(name);
                    MainActivity.data.get(position).setType(type);
                }

                MainActivity.myAdapter.notifyDataSetChanged();
                finish();

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                spinnerTypeValue = spinnerType.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        spinnerImg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                spinnerImgValue = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
    }

}


