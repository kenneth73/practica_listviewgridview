package com.example.practica_listviewgridview.models;

public class ProgrammingLanguage {
    private int idImage;
    private String name;
    private String type;

    public ProgrammingLanguage(int idImage, String name, String type) {
        this.idImage = idImage;
        this.name = name;
        this.type = type;
    }

    public int getIdImage() {
        return idImage;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public void setIdImage(int idImage) {
        this.idImage = idImage;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }
}
