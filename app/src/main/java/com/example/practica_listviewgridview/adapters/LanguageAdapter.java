package com.example.practica_listviewgridview.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.practica_listviewgridview.R;
import com.example.practica_listviewgridview.models.ProgrammingLanguage;

import java.util.ArrayList;

public class LanguageAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private ArrayList<ProgrammingLanguage> data;

    public LanguageAdapter(Context context, int layout, ArrayList<ProgrammingLanguage> data) {
        this.context = context;
        this.layout = layout;
        this.data = data;
    }

    @Override
    public int getCount() {
        return this.data.size();
    }

    @Override
    public Object getItem(int position) {
        return this.data.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //ViewHolder Pattern
        ViewHolder holder;

        if (convertView == null) {
            //Inflem la vista que ens ha arribat amb el nostre layout personalitzat
            LayoutInflater inflater = LayoutInflater.from(this.context);
            convertView = inflater.inflate(this.layout, null);

            holder = new ViewHolder();
            //Referenciem l'element a modificar i l'omplim
            holder.nameTextView = convertView.findViewById(R.id.textViewName);
            holder.typeTextView = convertView.findViewById(R.id.textViewType);
            holder.imageView = convertView.findViewById(R.id.imageView);

            //Guardem el holder a la vista
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //Portem el valor actual depenent de la posicio
        ProgrammingLanguage currentObject = (ProgrammingLanguage) getItem(position);

        //Referenciem l'element modificar i l'omplim
        holder.nameTextView.setText(currentObject.getName());
        holder.typeTextView.setText(currentObject.getType());
        holder.imageView.setImageResource(currentObject.getIdImage());

        return convertView;
    }
    static class ViewHolder{
        private TextView nameTextView;
        private TextView typeTextView;
        private ImageView imageView;
    }
}
